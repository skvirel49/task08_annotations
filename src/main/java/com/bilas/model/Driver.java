package com.bilas.model;

@FunctionalInterface
public interface Driver {
    void drive();
}
