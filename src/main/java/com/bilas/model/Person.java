package com.bilas.model;

import com.bilas.annotation.CustAnnotation;
import com.bilas.annotation.CustomAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.Objects;

public class Person implements Driver {
    private static final Logger LOGGER = LogManager.getLogger(Person.class);
    @CustAnnotation(name = "ivan")
    private String name;
    @CustomAnnotation
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String returnUpperCaseString(String str){
        String s = str.toUpperCase();
        return s;
    }

    public boolean isAgeBiggerTenEighteen(){
        if (this.age > 18){
            return true;
        }
        return false;
    }

    public int countBirthYear(int age){
        LocalDate localDate = LocalDate.now();
        return localDate.getYear() - age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public void drive() {
        LOGGER.info(this.name + " drive.");
    }
}
